<?php

use FennDooscar\AlgebraicCalculator\Equations\Quadratic;

require_once __DIR__ . '/../vendor/autoload.php';

setlocale(LC_ALL, 'en_US.UTF-8');

$a = readline('Параметр a: ');
$b = readline('Параметр b: ');
$c = readline('Параметр c: ');

$quadraticEquation = new Quadratic($a, $b, $c);
$quadraticRoots = $quadraticEquation->calculate();

Quadratic::printRoots($quadraticRoots);