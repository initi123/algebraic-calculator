<?php


namespace FennDooscar\AlgebraicCalculator\Dto\Quadratic;


class Roots
{
    /**
     * @var float|int
     */
    public float|int $firstRoot = 0;

    /**
     * @var float|int
     */
    public float|int $secondRoot = 0;
}