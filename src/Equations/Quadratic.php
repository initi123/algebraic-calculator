<?php


namespace FennDooscar\AlgebraicCalculator\Equations;

use FennDooscar\AlgebraicCalculator\Dto\Quadratic\Roots;
use FennDooscar\AlgebraicCalculator\Interfaces\Equations;

class Quadratic implements Equations
{
    /**
     * @var float|int
     */
    private float|int $a;

    /**
     * @var float|int
     */
    private float|int $b;

    /**
     * @var float|int
     */
    private float|int $c;

    /**
     * Quadratic constructor.
     * @param float|int $a
     * @param float|int $b
     * @param float|int $c
     */
    public function __construct(float|int $a, float|int $b, float|int $c)
    {
        $this->a = $a !== 0 ?: 1;
        $this->b = $b;
        $this->c = $c;
    }

    /**
     * Calculate equation
     *
     * @return mixed
     */
    public function calculate(): mixed
    {
        $discriminant = $this->getDiscriminant();

        return $this->getQuadraticRoots($discriminant);
    }

    /**
     * @param Roots $roots
     */
    public static function printRoots(Roots $roots): void
    {
        if (!$roots->firstRoot) {
            echo 'Дискриминант меньше нуля, корней нет' . PHP_EOL;

            return;
        }

        if (!$roots->secondRoot) {
            echo 'Дискриминант равен нулю, корень: ' . $roots->firstRoot . PHP_EOL;

            return;
        }

        echo 'Первый корень: ' . $roots->firstRoot . PHP_EOL . 'Второй корень: ' . $roots->secondRoot . PHP_EOL;
    }

    /**
     * @return float|int
     */
    private function getDiscriminant(): float|int
    {
        return ($this->b * $this->b) - 4 * ($this->a * $this->c);
    }

    /**
     * @param float|int $discriminant
     * @return Roots
     */
    private function getQuadraticRoots(float|int $discriminant): Roots
    {
        $roots = new Roots();

        if ($discriminant < 0) {
            return $roots;
        } elseif ($discriminant === 0) {
            $roots->firstRoot = (($this->b * -1) / (2 * $this->a));

            return $roots;
        }

        $roots->firstRoot = (($this->b * -1) - sqrt($discriminant)) / (2 * $this->a);
        $roots->secondRoot = (($this->b * -1) + sqrt($discriminant)) / (2 * $this->a);

        return $roots;
    }
}