<?php


namespace FennDooscar\AlgebraicCalculator\Interfaces;


interface Equations
{
    /**
     * Calculate equation
     *
     * @return mixed
     */
    public function calculate(): mixed;
}